# Google Photos Sort
A simple script to sort Google Photos export archives by months / years.
Developped in Python 2, tested on Unix environment.

## Usage

1. Follow those steps to download your Google Photos archives from Google : https://support.google.com/accounts/answer/3024190?hl=en
2. Modify the main.py script with your parameters :

```python
repertoire = "/Users/groussel/Downloads/GooglePhotos/"
archive = "takeout-20201125T1868759Z"
nbarchives = 9
anneemin = 2009
anneemax = 2020
```

2. Execute the script :

```python
python main.py
```

3. The script will create an arborescence and sort your Photos by months / years.
