#!/usr/bin/env python

import os

#############################################################
######################## PARAMETRAGE ########################
#############################################################

repertoire = "/Users/groussel/Downloads/GooglePhotos/"
archive = "takeout-20201125T1868759Z"
nbarchives = 9
anneemin = 2009
anneemax = 2020

#############################################################
#############################################################
################ NE PAS MODIFIER EN DESSOUS #################
#############################################################
#############################################################

nbarchives = nbarchives + 1
anneemax = anneemax + 1

# ETAPE0 : On extrait les archives

for x in range(1, nbarchives):
    if x < 10:
        x = "00" + str(x)
    elif x < 100:
        x = "0" + str(x)
    x = archive + "-" + x + ".zip"

    print("Extraction de l'archive {}".format(x))
    
    os.system('unzip -qq {}{}'.format(repertoire, x))

os.system('mv {}Takeout/Google\ Photos {}Takeout/ResteATraiter/ 2>/dev/null'.format(repertoire, repertoire))



# ETAPE1 : On surpprime les fichiers *.json

print("Suppression des fichiers *.json")

os.system('find {}Takeout/ -name "*.json" -type f -delete'.format(repertoire))

# ETAPE2 : On cree un repertoire par annees et on y deplace l'arborescence

for x in range(anneemin, anneemax):
    print("Traitement de l'annee {}".format(x))
    os.system('mkdir {}Takeout/{}'.format(repertoire, x))
    os.system('mv {}Takeout/ResteATraiter/{}-* {}Takeout/{} 2>/dev/null'.format(repertoire, x, repertoire, x))


# ETAPE3 : On cree un repertoire par mois et on y deplace l'arborescence

for annee in range(anneemin, anneemax): # GESTION DES ANNEES

    for m in range(1, 13): # GESTION DES MOIS
        if m == 1:
            mois = "Janvier"
        elif m == 2:
            mois = "Fevrier"
        elif m == 3:
            mois = "Mars"
        elif m == 4:
            mois = "Avril"
        elif m == 5:
            mois = "Mai"
        elif m == 6:
            mois = "Juin"
        elif m == 7:
            mois = "Juillet"
        elif m == 8:
            mois = "Aout"
        elif m == 9:
            mois = "Septembre"
        elif m == 10:
            mois = "Octobre"
        elif m == 11:
            mois = "Novembre"
        elif m == 12:
            mois = "Decembre"
        
        if m < 10:
            m = "0" + str(m)
        
        print("Traitement de {} {}".format(mois, annee))
        os.system('mkdir {}Takeout/{}/{}'.format(repertoire, annee, mois))

        for j in range(1, 32): # GESTION DES JOURS

            if j < 10:
                j = "0" + str(j)

            try:
                os.system('mv {}Takeout/{}/{}-{}-{}/* {}Takeout/{}/{}/ 2>/dev/null'.format(repertoire, annee, annee, m, j, repertoire, annee, mois))
            except:
                print("Erreur au jour : {} et mois : {}".format(j, m))

            for diese in range(0, 11):
                try:
                    os.system('mv {}Takeout/{}/{}-{}-{}\ #{}/* {}Takeout/{}/{}/ 2>/dev/null'.format(repertoire, annee, annee, m, j, diese, repertoire, annee, mois))
                except:
                    print("Erreur au jour : {} et mois : {}".format(j, m))


print("===============================================================================================================")
print("Nettoyage...")
os.system('find {}Takeout/ -type d -empty -delete'.format(repertoire))
print("Programme correctement termine !")
print("Certains repertoires necessitent un traitnement manuel. Voir {}Takeout/ResteATraiter".format(repertoire))
